﻿USE ciclistas;


-- Hoja 4 --------------------------------------------------------------------

-- Query 01 - Nombre y edad de los ciclistas que han ganado etapas
SELECT DISTINCT c.nombre, c.edad 
FROM 
  ciclista c 
  LEFT JOIN 
  etapa e 
  ON c.dorsal = e.dorsal;


-- Query 02 - Nombre y edad de los ciclistas que han ganado puertos
SELECT DISTINCT c.nombre, c.edad 
FROM 
  ciclista c 
  LEFT JOIN 
  puerto p 
  ON c.dorsal = p.dorsal;


-- Query 03 - Nombre y edad de los ciclistas que han ganado etapas y puertos
SELECT DISTINCT c.nombre, c.edad 
FROM 
  ciclista c 
  INNER JOIN 
  etapa e 
  ON c.dorsal = e.dorsal
  INNER JOIN
  puerto p 
  ON c.dorsal = p.dorsal
;


-- Query 04 - Listar el director de los equipos que tengan ciclistas que hayan ganado alguna etapa
SELECT DISTINCT eq.director
FROM 
  etapa et 
  INNER JOIN 
  ciclista c 
  INNER JOIN 
  equipo eq
  ON et.dorsal = c.dorsal AND c.nomequipo = eq.nomequipo;


-- Query 05 - Dorsal y nombre de los ciclistas que hayan ganado algún maillot
SELECT DISTINCT c.dorsal, c.nombre 
FROM 
  ciclista c 
  INNER JOIN 
  lleva l 
  ON c.dorsal = l.dorsal;


-- Query 06 - Dorsal y nombre de los ciclistas que hayan ganado el maillot amarillo
SELECT DISTINCT c.dorsal, c.nombre 
FROM 
  ciclista c 
  INNER JOIN 
  lleva l 
  ON c.dorsal = l.dorsal
  INNER JOIN
  maillot m ON l.código = m.código
WHERE m.color = 'amarillo';


-- Query 07 - Dorsal y nombre de los ciclistas que hayan ganado algún maillot y halla ganado etapas
SELECT DISTINCT c.dorsal, c.nombre 
FROM 
  ciclista c 
  INNER JOIN 
  lleva l 
  ON c.dorsal = l.dorsal
  INNER JOIN
  etapa e ON c.dorsal = e.dorsal;


-- Query 08 - Número de etapa de las etapas con puertos
SELECT DISTINCT e.numetapa
FROM 
  etapa e 
  INNER JOIN 
  puerto p 
  ON e.numetapa = p.numetapa;


-- Query 09 - Kms de las etapas que hayan sido ganadas por ciclistas de Banesto
SELECT e.kms 
FROM 
  ciclista c 
  INNER JOIN 
  etapa e 
  ON c.dorsal = e.dorsal 
WHERE c.nomequipo = 'BANESTO';

SELECT SUM(e.kms)
FROM 
  ciclista c 
  INNER JOIN 
  etapa e 
  ON c.dorsal = e.dorsal 
WHERE c.nomequipo = 'BANESTO';


-- Query 10 - NÚmero de ciclistas que hayan ganado alguna etapa con puerto
SELECT COUNT(DISTINCT c.dorsal) 
FROM 
  ciclista c 
  INNER JOIN 
  etapa e 
  ON c.dorsal = e.dorsal 
  INNER JOIN 
  puerto p 
  ON e.numetapa = p.numetapa;


-- Query 11 - Nombres de los puertos ganados por ciclistas de Banesto
SELECT p.nompuerto 
FROM 
  ciclista c 
  INNER JOIN 
  puerto p 
  ON c.dorsal = p.dorsal 
WHERE c.nomequipo = 'BANESTO';


-- Query 12 - Número de etapas con puertos ganados por ciclistas de Banesto de mas de 200Km
SELECT COUNT(*)
FROM 
  etapa e 
  INNER JOIN 
  puerto p 
  ON e.numetapa = p.numetapa
  INNER JOIN 
  ciclista c
  ON p.dorsal = c.dorsal 
WHERE c.nomequipo='BANESTO' AND e.kms>200;