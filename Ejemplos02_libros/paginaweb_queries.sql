USE ejemplos_m1u2ej02_paginaWeb;

SELECT l.titulo, a.nombre_completo
FROM libro l, autor a
ORDER BY l.titulo ASC, a.nombre_completo ASC;


-- 1 - Libros y su correspondiente autor

SELECT l.titulo, a.nombre_completo
FROM libro l INNER JOIN autor a
ON l.autor = a.id_autor
ORDER BY l.titulo ASC, a.nombre_completo ASC;

SELECT l.titulo, a.nombre_completo
FROM libro l, autor a
WHERE l.autor = a.id_autor
ORDER BY l.titulo ASC, a.nombre_completo ASC;



-- 2 - Autores y libros en que ayudan

SELECT l.titulo, a.nombre_completo AS escritor_asistente
FROM 
  libro l 
  INNER JOIN ayuda y ON l.id_libro = y.libro 
  INNER JOIN autor a ON y.autor = a.id_autor
ORDER BY l.titulo ASC, a.nombre_completo ASC;
  
SELECT l.titulo, a.nombre_completo AS escritor_asistente
FROM 
  libro l INNER JOIN ayuda y INNER JOIN autor a 
  ON l.id_libro = y.libro AND y.autor = a.id_autor
ORDER BY l.titulo ASC, a.nombre_completo ASC;

      
SELECT l.titulo, a.nombre_completo AS escritor_asistente
FROM libro l, ayuda y, autor a 
WHERE l.id_libro = y.libro AND y.autor = a.id_autor
ORDER BY l.titulo ASC, a.nombre_completo ASC;

-- Ejemplo de optimizaci�n
SELECT asistenteDeLibro.titulo, a.nombre_completo AS escritor_ayudante
FROM 
  (
    SELECT l.titulo, y.autor AS ayudante
    FROM libro l INNER JOIN ayuda y ON l.id_libro = y.libro 
  ) asistenteDeLibro
  INNER JOIN autor a ON asistenteDeLibro.ayudante = a.id_autor
ORDER BY asistenteDeLibro.titulo ASC, a.nombre_completo ASC;



-- 3 - Libros que se han descargado con su id, fecha y login de usuario
SELECT des.libro AS id_libro, des.usuario AS login_usuario, desf.fecha_descarga
  FROM descarga des 
    INNER JOIN fechadescarga desf ON des.libro = desf.libro AND des.usuario = desf.usuario;

-- Como todo esta en fecha de descarga porque no hay que relacionar, quedate solo con fecha de descarga

SELECT desf.libro AS id_libro, desf.usuario AS login_usuario, desf.fecha_descarga
  FROM fechadescarga desf;



-- 4 - Libros que se han descargado con login y email de usuario, fecha de descarga y t�tulo
SELECT
  l.titulo, 
  des.usuario AS login,
  u.email, 
  f.fecha_descarga
FROM descarga des
  INNER JOIN fechadescarga f ON des.libro = f.libro AND des.usuario = f.usuario
  INNER JOIN usuario u ON u.login = des.usuario
  INNER JOIN libro l ON l.id_libro = des.libro
ORDER BY l.titulo ASC, des.usuario ASC, f.fecha_descarga ASC;


SELECT
  l.titulo, 
  des.usuario AS login,
  u.email, 
  f.fecha_descarga
FROM descarga des INNER JOIN fechadescarga f INNER JOIN usuario u INNER JOIN libro l
  ON des.libro = f.libro AND des.usuario = f.usuario AND u.login = des.usuario AND l.id_libro = des.libro
ORDER BY l.titulo ASC, des.usuario ASC, f.fecha_descarga ASC;

SELECT
  l.titulo, 
  des.usuario AS login,
  u.email, 
  f.fecha_descarga
FROM descarga des INNER JOIN fechadescarga f INNER JOIN usuario u INNER JOIN libro l
  WHERE des.libro = f.libro AND des.usuario = f.usuario AND u.login = des.usuario AND l.id_libro = des.libro
ORDER BY l.titulo ASC, des.usuario ASC, f.fecha_descarga ASC;

SELECT
  l.titulo, 
  des.usuario AS login,
  u.email, 
  f.fecha_descarga
FROM descarga des, fechadescarga f, usuario u, libro l
  WHERE des.libro = f.libro AND des.usuario = f.usuario AND u.login = des.usuario AND l.id_libro = des.libro
ORDER BY l.titulo ASC, des.usuario ASC, f.fecha_descarga ASC;

-- 5 - N�MERO DE LIBROS QUE HAY

SELECT COUNT(*) 
FROM libro;


-- 6 - N�MERO DE LIBROS QUE HAY POR COLECCI�N (NO HACE FALTA EL NOMBRE)
SELECT 
  l.coleccion, COUNT(*) AS nlibros
FROM libro l
GROUP BY l.coleccion;

-- 7 - COLECCI�N(ES) CON MAYOR N�MERO DE LIBROS (NO HACE FALTA EL NOMBRE)

-- Sin JOIN
SELECT 
  l.coleccion -- , COUNT(*) AS nlibros
FROM libro l
GROUP BY l.coleccion
HAVING COUNT(*) = (
  SELECT MAX(ll.nlibros)
  FROM (
    SELECT 
      COUNT(*) AS nlibros
    FROM libro l
    GROUP BY l.coleccion
  ) AS ll
);

-- Con JOIN
SELECT totales.coleccion
FROM (
  SELECT 
    l.coleccion, COUNT(*) AS nlibros
  FROM libro l
  GROUP BY l.coleccion
) AS totales
INNER JOIN
(
  SELECT MAX(maximos.nlibros) AS maximo
  FROM (
    SELECT 
      COUNT(*) AS nlibros
    FROM libro l
    GROUP BY l.coleccion
  ) AS maximos
) AS maximos
ON totales.nlibros = maximos.maximo;

SELECT totales.coleccion
FROM (
  SELECT 
    l.coleccion, COUNT(*) AS nlibros
  FROM libro l
  GROUP BY l.coleccion
) AS totales
INNER JOIN
(
  SELECT MIN(minimos.nlibros) AS minimo
  FROM (
    SELECT COUNT(*) AS nlibros
    FROM libro l
    GROUP BY l.coleccion
  ) AS minimos
) AS minimo
ON totales.nlibros = minimo.minimo;



-- 8 - COLECCI�N(ES) CON MENOE N�MERO DE LIBROS (NO HACE FALTA EL NOMBRE)

-- Sin JOIN

SELECT 
  l.coleccion -- , COUNT(*) AS nlibros
FROM libro l
GROUP BY l.coleccion
HAVING COUNT(*) = (
  SELECT MIN(ll.nlibros)
  FROM (
    SELECT 
      COUNT(*) AS nlibros
    FROM libro l
    GROUP BY l.coleccion
  ) AS ll
);

-- Con JOIN
SELECT totales.coleccion
FROM (
  SELECT 
    l.coleccion, COUNT(*) AS nlibros
  FROM libro l
  GROUP BY l.coleccion
) AS totales
INNER JOIN
(
  SELECT MIN(minimos.nlibros) AS minimo
  FROM (
    SELECT 
      COUNT(*) AS nlibros
    FROM libro l
    GROUP BY l.coleccion
  ) AS minimos
) AS minimos
ON totales.nlibros = minimos.minimo;

SELECT *
FROM (
  SELECT l.coleccion, COUNT(*) AS nlibros
  FROM libro l
  GROUP BY l.coleccion
) totales
WHERE totales.nlibros = (
  SELECT MIN(ll.nlibros)
  FROM (
    SELECT COUNT(*) AS nlibros
    FROM libro l
    GROUP BY l.coleccion
  ) AS ll
); 



-- 9 - NOMBRE DE LA COLECCION QUR TIENE MAS LIBROS
SELECT coleccion.nombre
FROM 
  coleccion
INNER JOIN
(
  SELECT 
    l.coleccion, COUNT(*) AS nlibros
  FROM libro l
  GROUP BY l.coleccion
) AS totales
ON coleccion.id_coleccion=totales.coleccion
INNER JOIN
(
  SELECT MAX(maximos.nlibros) AS maximo
  FROM (
    SELECT COUNT(*) AS nlibros
    FROM libro l
    GROUP BY l.coleccion
  ) AS maximos
) AS maximo
ON totales.nlibros = maximo.maximo;

SELECT col.nombre
FROM coleccion col
WHERE col.id_coleccion IN (
  SELECT l.coleccion
  FROM libro l
  GROUP BY l.coleccion
  HAVING COUNT(*) = (
    SELECT MAX(ll.nlibros)
    FROM (
      SELECT COUNT(*) AS nlibros
      FROM libro l
      GROUP BY l.coleccion
    ) AS ll
  )
);


SELECT col.nombre
FROM coleccion col
WHERE col.id_coleccion IN (
  SELECT totales.coleccion
  FROM (
    SELECT l.coleccion, COUNT(*) AS nlibros
    FROM libro l
    GROUP BY l.coleccion
  ) totales
  WHERE totales.nlibros = (
    SELECT MAX(ll.nlibros)
    FROM (
      SELECT COUNT(*) AS nlibros
      FROM libro l
      GROUP BY l.coleccion
    ) AS ll
  )
); 


-- 10 - 
SELECT coleccion.nombre
FROM coleccion
INNER JOIN
(
  SELECT 
    l.coleccion, COUNT(*) AS nlibros
  FROM libro l
  GROUP BY l.coleccion
) AS totales
ON coleccion.id_coleccion=totales.coleccion
INNER JOIN
(
  SELECT MIN(minimos.nlibros) AS valor
  FROM (
    SELECT COUNT(*) AS nlibros
    FROM libro l
    GROUP BY l.coleccion
  ) AS minimos
) AS minimo
ON totales.nlibros = minimo.valor;


SELECT col.nombre
FROM coleccion col
WHERE col.id_coleccion IN (
  SELECT l.coleccion
  FROM libro l
  GROUP BY l.coleccion
  HAVING COUNT(*) = (
    SELECT MIN(ll.nlibros)
    FROM (
      SELECT COUNT(*) AS nlibros
      FROM libro l
      GROUP BY l.coleccion
    ) AS ll
  )
);

SELECT col.nombre
FROM coleccion col
WHERE col.id_coleccion IN (
  SELECT totales.coleccion
  FROM (
    SELECT l.coleccion, COUNT(*) AS nlibros
    FROM libro l
    GROUP BY l.coleccion
  ) totales
  WHERE totales.nlibros = (
    SELECT MIN(ll.nlibros)
    FROM (
      SELECT COUNT(*) AS nlibros
      FROM libro l
      GROUP BY l.coleccion
    ) AS ll
  )
); 


-- 11 - 
SELECT fd.libro, COUNT(*) AS ndescargas
FROM fechaDescarga fd
GROUP BY fd.libro;

SELECT MAX(nd.ndescargas) AS maximo
FROM (
  SELECT fd.libro, COUNT(*) AS ndescargas
  FROM fechaDescarga fd
  GROUP BY fd.libro
) AS nd;


SELECT descargas.libro
FROM 
  (
    SELECT fd.libro, COUNT(*) AS ndescargas
    FROM fechaDescarga fd
    GROUP BY fd.libro
  ) AS descargas
  INNER JOIN 
  (
    SELECT MAX(nd.ndescargas) AS valor
    FROM 
    (
      SELECT fd.libro, COUNT(*) AS ndescargas
      FROM fechaDescarga fd
      GROUP BY fd.libro
    ) AS nd
  ) AS maximo
  ON descargas.ndescargas = maximo.valor;


SELECT l.titulo
FROM 
  libro l
  INNER JOIN
  (
    SELECT fd.libro, COUNT(*) AS ndescargas
    FROM fechaDescarga fd
    GROUP BY fd.libro
  ) AS descargas
  ON l.id_libro = descargas.libro
  INNER JOIN 
  (
    SELECT MAX(nd.ndescargas) AS valor
    FROM 
    (
      SELECT fd.libro, COUNT(*) AS ndescargas
      FROM fechaDescarga fd
      GROUP BY fd.libro
    ) AS nd
  ) AS maximo
  ON descargas.ndescargas = maximo.valor;



-- 12 - El nombre de los usuarios que han descargado m�s libros
SELECT fd.usuario, COUNT(*) AS ndescargas 
FROM fechadescarga fd
GROUP BY fd.usuario;


SELECT MAX(nd.ndescargas) AS maximo
FROM (
  SELECT fd.usuario, COUNT(*) AS ndescargas 
  FROM fechadescarga fd
  GROUP BY fd.usuario
) AS nd;

SELECT descargas.usuario
FROM 
(
  SELECT fd.usuario, COUNT(*) AS ndescargas 
  FROM fechadescarga fd
  GROUP BY fd.usuario
) AS descargas
INNER JOIN 
(
  SELECT MAX(nd.ndescargas) AS valor
  FROM 
  (
    SELECT fd.usuario, COUNT(*) AS ndescargas 
    FROM fechadescarga fd
    GROUP BY fd.usuario
  ) AS nd
) AS maximo
ON descargas.ndescargas = maximo.valor;


SELECT u.email
FROM 
  usuario u
INNER JOIN
(
  SELECT fd.usuario, COUNT(*) AS ndescargas 
  FROM fechadescarga fd
  GROUP BY fd.usuario
) descargas
ON u.login = descargas.usuario
INNER JOIN
(
  SELECT MAX(nd.ndescargas) AS valor
  FROM (
    SELECT fd.usuario, COUNT(*) AS ndescargas 
    FROM fechadescarga fd
    GROUP BY fd.usuario
  ) AS nd
) AS maximo
ON descargas.ndescargas = maximo.valor;


SELECT fd.usuario, COUNT(*) ndescargas
FROM fechadescarga fd GROUP BY fd.usuario
HAVING ndescargas = (
  SELECT MAX(nd.ndescargas)
  FROM (
    SELECT fd.usuario, COUNT(*) AS ndescargas 
    FROM fechadescarga fd GROUP BY fd.usuario
  ) AS nd
);
 
SELECT u.email
FROM 
  usuario u
  INNER JOIN
  (
    SELECT descargas.usuario
    FROM 
    (
      SELECT fd.usuario, COUNT(*) AS ndescargas 
      FROM fechadescarga fd
      GROUP BY fd.usuario
    ) AS descargas
    INNER JOIN
    (
      SELECT MAX(nd.ndescargas) AS valor
      FROM (
        SELECT fd.usuario, COUNT(*) AS ndescargas 
        FROM fechadescarga fd
        GROUP BY fd.usuario
      ) AS nd
    ) AS maximo
    ON descargas.ndescargas = maximo.valor
  ) AS tope
  ON u.login = tope.usuario;


-- 13 - Descargas de los usuarios que han descargado m�s libros que Adam3

SELECT fd.usuario, COUNT(*) AS ndescargas 
FROM fechadescarga fd
GROUP BY fd.usuario;

SELECT COUNT(*) AS descargasAdam3 
FROM fechadescarga fd
WHERE fd.usuario = "Adam3";

-- Con Where

SELECT nd.usuario
FROM 
( SELECT fd.usuario, COUNT(*) AS ndescargas 
  FROM fechadescarga fd
  GROUP BY fd.usuario
) AS nd
WHERE nd.ndescargas > 
( SELECT COUNT(*) AS descargasAdam3 
  FROM fechadescarga fd
  WHERE fd.usuario = "Adam3"
);

-- Con having

SELECT fd.usuario
FROM fechadescarga fd
GROUP BY fd.usuario
HAVING COUNT(*) > ( 
  SELECT COUNT(*) AS descargasAdam3 
  FROM fechadescarga fda
  WHERE fda.usuario = "Adam3"
);

-- Lo mismo opero devolviendo emails

SELECT u.email
FROM 
  usuario u
INNER JOIN
( SELECT nd.usuario
  FROM 
  ( SELECT fd.usuario, COUNT(*) AS ndescargas 
    FROM fechadescarga fd
    GROUP BY fd.usuario
  ) AS nd
  WHERE nd.ndescargas > 
  ( SELECT COUNT(*) AS descargasAdam3 
    FROM fechadescarga fd
    WHERE fd.usuario = "Adam3"
  )
) AS tope
ON u.login = tope.usuario;
SELECT u.email
FROM 
  usuario u
INNER JOIN
(
  SELECT fd.usuario
  FROM fechadescarga fd
  GROUP BY fd.usuario
  HAVING COUNT(*) > ( 
    SELECT COUNT(*) AS descargasAdam3 
    FROM fechadescarga fda
    WHERE fda.usuario = "Adam3"
  )
) AS tope
ON u.login = tope.usuario;

SELECT u.email
FROM 
  usuario u
INNER JOIN
( SELECT descargas.usuario
  FROM
  ( SELECT fd.usuario, COUNT(*) AS ndescargas 
    FROM fechadescarga fd
    GROUP BY fd.usuario
  ) AS descargas
  INNER JOIN
  ( SELECT MAX(nd.ndescargas) AS valor
    FROM 
    ( SELECT fd.usuario, COUNT(*) AS ndescargas 
      FROM fechadescarga fd
      GROUP BY fd.usuario
    ) AS nd
  ) AS maximo
  ON descargas.ndescargas = maximo.valor
) AS tope
ON u.login = tope.usuario;




-- 14 - El mes en que m�s libros se han descargado
SELECT MONTH(fd.fecha_descarga) AS mes, COUNT(*) AS ndescargas 
FROM fechadescarga fd
GROUP BY MONTH(fd.fecha_descarga);

SELECT MAX(fd.ndescargas) AS top_month
FROM (
  SELECT MONTH(fd.fecha_descarga) AS mes, COUNT(*) AS ndescargas 
  FROM fechadescarga fd
  GROUP BY MONTH(fd.fecha_descarga)
) AS fd;


-- HAVING
SELECT MONTH(fd.fecha_descarga) AS mes
FROM fechadescarga fd
GROUP BY MONTH(fd.fecha_descarga)
HAVING COUNT(*) = (
  SELECT MAX(fd.ndescargas) AS top_month
  FROM (
    SELECT MONTH(fd.fecha_descarga) AS mes, COUNT(*) AS ndescargas 
    FROM fechadescarga fd
    GROUP BY MONTH(fd.fecha_descarga)
  ) AS fd
);


-- WHERE
SELECT descargasPorMes.mes
FROM 
  (
    SELECT MONTH(fd.fecha_descarga) AS mes, COUNT(*) AS ndescargas 
    FROM fechadescarga fd
    GROUP BY MONTH(fd.fecha_descarga)
  ) AS descargasPorMes  
WHERE ndescargas = (
  SELECT MAX(fd.ndescargas) AS top_month
  FROM (
    SELECT MONTH(fd.fecha_descarga) AS mes, COUNT(*) AS ndescargas 
    FROM fechadescarga fd
    GROUP BY MONTH(fd.fecha_descarga)
  ) AS fd
);

-- INNER JOIN
SELECT descargaPorMes.mes
FROM 
  (
    SELECT MONTH(fd.fecha_descarga) AS mes, COUNT(*) AS ndescargas 
    FROM fechadescarga fd
    GROUP BY MONTH(fd.fecha_descarga)
  ) AS descargaPorMes
  INNER JOIN
  (
    SELECT MAX(fd.ndescargas) AS top_month
    FROM (
      SELECT MONTH(fd.fecha_descarga) AS mes, COUNT(*) AS ndescargas 
      FROM fechadescarga fd
      GROUP BY MONTH(fd.fecha_descarga)
    ) AS fd
  ) AS fd
  ON descargaPorMes.ndescargas = fd.top_month;

-- Producto cartesiano
SELECT descargaPorMes.mes
FROM 
  (
    SELECT MONTH(fd.fecha_descarga) AS mes, COUNT(*) AS ndescargas 
    FROM fechadescarga fd
    GROUP BY MONTH(fd.fecha_descarga)
  ) AS descargaPorMes
  ,
  (
    SELECT MAX(fd.ndescargas) AS top_month
    FROM (
      SELECT MONTH(fd.fecha_descarga) AS mes, COUNT(*) AS ndescargas 
      FROM fechadescarga fd
      GROUP BY MONTH(fd.fecha_descarga)
    ) AS fd
  ) AS fd
  WHERE descargaPorMes.ndescargas = fd.top_month;



