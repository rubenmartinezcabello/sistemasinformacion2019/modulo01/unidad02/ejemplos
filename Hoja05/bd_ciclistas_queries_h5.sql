﻿USE ciclistas;

-- Hoja 5 --------------------------------------------------------------------
SELECT DISTINCT e.dorsal
FROM 
  etapa e;


-- Query 01 - Nombre y edad de los ciclistas que no han ganado etapas.
SELECT DISTINCT c.nombre, c.edad
FROM 
  ciclista c 
  LEFT JOIN 
  etapa e 
  ON c.dorsal = e.dorsal
WHERE e.numetapa IS NULL;


-- Query 02 - Nombre y edad de los ciclistas que no han ganado puertos.
SELECT DISTINCT c.dorsal, c.nombre, c.edad
FROM 
  ciclista c 
  LEFT JOIN 
  puerto p 
  ON c.dorsal = p.dorsal
WHERE p.nompuerto IS NULL;


-- Query 03 - Listar el director de los equipos que tengan ciclistas que NO hayan ganado ninguna etapa.
SELECT DISTINCT c.nomequipo, eq.director
FROM 
  ciclista c 
  LEFT JOIN 
  etapa e ON c.dorsal = e.dorsal
  INNER JOIN 
  equipo eq ON c.nomequipo = eq.nomequipo
WHERE e.numetapa IS NOT NULL;


SELECT DISTINCT c.nomequipo, eq.director
FROM 
  ( SELECT DISTINCT c.nomequipo
    FROM 
      ciclista c 
      LEFT JOIN 
      etapa e ON c.dorsal = e.dorsal
    WHERE e.numetapa IS NOT NULL
  ) c
  INNER JOIN 
  equipo eq ON c.nomequipo = eq.nomequipo;


-- Query 04 - Dorsal y nombre de los ciclistas que NO HAN ganado algún maillot.
SELECT DISTINCT c.dorsal, c.nombre
FROM 
  ciclista c 
  LEFT JOIN
  lleva ll
  ON c.dorsal = ll.dorsal
WHERE ll.numetapa IS NULL;


-- Query 05 - Dorsal y nombre de los ciclistas que NO HAN llevado el maillot amarillo.
SELECT DISTINCT c.dorsal, c.nombre
FROM 
  ciclista c 
  LEFT JOIN
  (
    SELECT DISTINCT `dorsal`
    FROM
      lleva ll
      INNER JOIN
      maillot m 
      ON ll.código = m.código
      WHERE m.color = 'amarillo'
  ) ganadores
  ON c.dorsal = ganadores.dorsal
WHERE ganadores.dorsal IS NULL;


-- Query 06 - Indica en número de etapas que NO tengan puertos.
SELECT DISTINCT numetapa FROM puerto p;

SELECT e.numetapa 
FROM etapa e 
WHERE e.numetapa NOT IN (
  SELECT DISTINCT numetapa 
  FROM puerto p
);

SELECT e.numetapa 
FROM 
  etapa e
  LEFT JOIN 
  puerto p
  ON e.numetapa = p.numetapa
WHERE p.numetapa IS NULL;


-- Query 07 - Indica la distancia media de las etapas que no tengan puertos.
SELECT * -- e.numetapa
FROM 
  etapa e
  LEFT JOIN 
  puerto p
  ON e.numetapa = p.numetapa
;
  /* 
WHERE (e.salida != e.llegada) AND e.dorsal IS NOT NULL;
*/