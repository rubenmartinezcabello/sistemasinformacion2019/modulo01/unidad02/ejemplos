﻿USE ciclistas;


-- Hoja 3 --------------------------------------------------------------------

-- Query 01 - Listar las edades de todos los ciclistas de Banesto
SELECT DISTINCT
  c.edad
FROM
  ciclista c
WHERE
  nomequipo LIKE 'BANESTO';


-- Query 02 - Listar las edades de todos los ciclistas de Banesto y Navigare
SELECT DISTINCT
  c.edad
FROM
  ciclista c
WHERE
  nomequipo LIKE 'BANESTO' OR nomequipo LIKE 'NAVIGARE';

SELECT DISTINCT
  c.edad
FROM
  ciclista c
WHERE
  nomequipo LIKE 'BANESTO'
UNION
SELECT DISTINCT
  c.edad
FROM
  ciclista c
WHERE
  nomequipo LIKE 'NAVIGARE';


-- Query 03 - Dorsal de los ciclistas de Banesto y cuya edad esté entre 25 y 32 años
SELECT
  c.dorsal
FROM
  ciclista c
WHERE c.edad BETWEEN 28 AND 32
INTERSECT
SELECT
  c.dorsal
FROM
  ciclista c
WHERE c.nomequipo = 'Banesto';

SELECT
  c.dorsal
FROM
  ciclista c
WHERE c.edad BETWEEN 28 AND 32 AND c.nomequipo LIKE 'Banesto';


-- Query 04 - Dorsal de los ciclistas de Banesto o cuya edad esté entre 25 y 32 años
SELECT
  c.dorsal
FROM
  ciclista c
WHERE c.edad BETWEEN 28 AND 32
UNION
SELECT
  c.dorsal
FROM
  ciclista c
WHERE c.nomequipo = 'Banesto';

SELECT
  c.dorsal
FROM
  ciclista c
WHERE c.edad BETWEEN 28 AND 32 OR c.nomequipo LIKE 'Banesto';


-- Query 05 - Listar la inicial del equipo de los ciclistas cuyo nombre empieza por R
SELECT DISTINCT SUBSTR(c.nomequipo,1,1) 
FROM ciclista c 
WHERE c.nombre LIKE 'R%';


-- Query 06 - Listar el código de las etapas que su salida y llegada sea en la misma población
SELECT e.numetapa
FROM etapa e 
WHERE e.salida = e.llegada;


-- Query 07 - Listar el código de las epatas que su salida y llegada NO sea en la misma población, y que conozcamos el dorsal del ciclista ganador
SELECT e.numetapa
FROM etapa e 
WHERE (e.salida != e.llegada) AND e.dorsal IS NOT NULL;


-- Query 08 - Listar los nombres de puertos entre 1000 y 2000 metros o de más de 2400m
SELECT p.nompuerto 
FROM puerto p 
WHERE (altura>1000) AND (altura<2000) OR (altura>2400);

SELECT p.nompuerto 
FROM puerto p 
WHERE (altura BETWEEN 1000 AND 2000) OR (altura>2400);


-- Query 09 - Listar el dorsal de los ciclistas que hayan ganado los puertos entre 1000 y 2000 metros o de mas de 2400m
SELECT DISTINCT p.dorsal
FROM puerto p 
WHERE (altura>1000) AND (altura<2000) OR (altura>2400);

SELECT DISTINCT p.dorsal
FROM puerto p 
WHERE (altura BETWEEN 1000 AND 2000) OR (altura>2400);


-- Query 10 - Listar el número de ciclistas que han ganado alguna etapa
SELECT COUNT(DISTINCT e.dorsal)
FROM etapa e 
WHERE e.dorsal IS NOT null;


-- Query 11 - Listar el número de etapas que contengan puerto
SELECT COUNT(DISTINCT p.numetapa) 
FROM puerto p;


-- Query 12 - Listar el número de ciclistas que han ganado algun puerto
SELECT COUNT(DISTINCT p.dorsal) 
FROM puerto p;


-- Query 13 - Listar el código de la etapa con el número de puertos que contiene 
SELECT p.numetapa, COUNT(*) AS countetapa
FROM puerto p
GROUP BY p.numetapa;



-- Query 14 - Altura media de los puertos
SELECT AVG(p.altura) 
FROM puerto p;


-- Query 15 - Código de la etapa cuya altura media de sus puertos esta por encima de 1500m
SELECT p.numetapa, AVG(p.altura) AS alturamedia 
FROM puerto p
GROUP BY p.numetapa
HAVING alturamedia > 1500;


-- Query 16 - Numero de etapas que cumplen lo anterior
SELECT COUNT(*)
FROM 
(
  SELECT p.numetapa, AVG(p.altura) AS alturamedia 
  FROM puerto p
  GROUP BY p.numetapa
  HAVING alturamedia > 1500
) AS alturasmedias;


-- Query 17 - Dorsal del ciclista con el número de veces que han llevado algun maillot
SELECT l.dorsal, COUNT(*) AS veces
FROM lleva l
GROUP BY l.dorsal;


-- Query 18 - Listar el dorsal del ciclista con el código del maillot y cuentas veces lo han llevado
SELECT l.dorsal, l.código, COUNT(*) AS veces
FROM lleva l
GROUP BY l.dorsal, l.código;



-- Query 19 - Lista el dorsal, código de etapa ciclista y número de maiyots que el ciclista ha llevado en cada etapa
SELECT l.dorsal, l.numetapa, COUNT(*) AS veces
FROM lleva l
GROUP BY l.dorsal, l.numetapa;
;