﻿DROP DATABASE IF EXISTS ejemplo2_colegio;
CREATE DATABASE ejemplo2_colegio;
USE ejemplo2_colegio;

CREATE OR REPLACE TABLE cursos (          
  id int AUTO_INCREMENT , 
  nombre varchar(64),
  categoria varchar(16), /* --enum (primaria, secundaria, fp, grado), */
  grado varchar (60),
  PRIMARY KEY (id)
  );
CREATE OR REPLACE TABLE profesores (          
  id int AUTO_INCREMENT , 
  nombre varchar(64),
  apellidos varchar(64), 
  especialidad varchar (60),
  PRIMARY KEY (id)
  );
CREATE OR REPLACE TABLE alumnos (          
  id int AUTO_INCREMENT , 
  nombre varchar(64),
  apellidos varchar(64),
  f_nac date,
  PRIMARY KEY (id)
  );
CREATE OR REPLACE TABLE ediciones (          
  id_curso int,
  f_comienzo date,
  PRIMARY KEY (id_curso, f_comienzo),
    CONSTRAINT ediciones_ibfk_1 FOREIGN KEY (id_curso) REFERENCES cursos(id)
  );
CREATE OR REPLACE TABLE imparten (
  id_profesor int,          
  id_curso int,
  f_comienzo date,
  PRIMARY KEY (id_profesor, id_curso, f_comienzo),
  CONSTRAINT imparten_ibfk_1 FOREIGN KEY (id_profesor) REFERENCES profesores(id),
  CONSTRAINT imparten_ibfk_2 FOREIGN KEY (id_curso, f_comienzo) REFERENCES ediciones(id_curso, f_comienzo)
  
  );
    CREATE OR REPLACE TABLE cursan (          
  id_curso int,
  f_comienzo date,
      id_alumno int,
      nota float (5,2),
                
  PRIMARY KEY (id_curso, f_comienzo, id_alumno),
  UNIQUE KEY (id_alumno),
  CONSTRAINT cursan_ibfk_1 FOREIGN KEY (id_alumno) REFERENCES alumnos(id),
  CONSTRAINT cursan_ibfk_2 FOREIGN KEY (id_curso, f_comienzo) REFERENCES ediciones(id_curso, f_comienzo)

    );





           
                 