﻿                                                      /** MODULO I - UNIDAD 2 - EJEMPLO LIGA DE PESCADORES **/

-- creando Base de Datos
DROP DATABASE IF EXISTS ejemplos_m1u2ej02_pesca;
CREATE DATABASE ejemplos_m1u2ej02_pesca;
USE ejemplos_m1u2ej02_pesca;


-- creando Tablas
CREATE OR REPLACE TABLE clubes(
  cif varchar (10),
  nombre varchar (50),
  provincia varchar (20),
  PRIMARY KEY (cif)
);


CREATE OR REPLACE TABLE pescadores(
  numSocio int AUTO_INCREMENT,
  dni varchar (10),
  nombre varchar (50),
  tipoCarnet varchar (10),
  fechaAlta date,
  club_cif varchar (10),
  PRIMARY KEY (numSocio)
);

CREATE OR REPLACE TABLE cotos(
  nombre varchar (50),
  provincia varchar (20),
  rio varchar (50),
  PRIMARY KEY (nombre)
);

CREATE OR REPLACE TABLE autorizados(
  club_cif varchar (10),
  coto_nombre varchar (50),
  PRIMARY KEY (club_cif, coto_nombre)
);

CREATE OR REPLACE TABLE apadrinar(
  padrino int,
  ahijado int,
  PRIMARY KEY (padrino,ahijado),
  UNIQUE KEY (ahijado)
);


-- creando Claves Ajenas
ALTER TABLE pescadores
ADD CONSTRAINT fk_pescadoresClubes
FOREIGN KEY (club_cif) REFERENCES clubes(cif)
;


ALTER TABLE autorizados
ADD CONSTRAINT fk_autorizadosClub
FOREIGN KEY (club_cif) REFERENCES clubes(cif),

ADD CONSTRAINT fk_autorizadosCotos
FOREIGN KEY (coto_nombre) REFERENCES cotos(nombre)
;

ALTER TABLE apadrinar
ADD CONSTRAINT fk_padrinoPescadores
FOREIGN KEY (padrino) REFERENCES pescadores(numSocio)
;


ALTER TABLE apadrinar
ADD CONSTRAINT fk_ahijadoPescadores
FOREIGN KEY (ahijado) REFERENCES pescadores(numSocio)
;