--
-- Script was generated by Devart dbForge Studio for MySQL, Version 8.0.40.0
-- Product Home Page: http://www.devart.com/dbforge/mysql/studio
-- Script date 12/07/2019 14:04:24
-- Target server version: 5.5.5-10.1.40-MariaDB
-- Target connection string: User Id=root;Host=127.0.0.1;Character Set=utf8
--



SET NAMES 'utf8';
USE ejemplos_m1u2ej02_pesca;
--

-- 1
SELECT DISTINCT
  c.nombre
FROM
  Clubes c
INNER JOIN
  Pescadores p
ON c.cif = p.club_cif;


-- 2
SELECT 
  c.nombre
FROM
  Clubes c
LEFT JOIN
  Pescadores p
ON c.cif = p.club_cif
WHERE p.club_cif IS NULL;


-- 3 
SELECT DISTINCT coto_nombre AS nombre
FROM autorizados;


-- 4 
SELECT DISTINCT provincia
FROM 
  autorizados a
INNER JOIN
  cotos c
ON a.coto_nombre = c.nombre;


-- 5 -- 
SELECT DISTINCT c.nombre 
FROM 
  cotos c
LEFT JOIN
  autorizados a
ON c.nombre = a.coto_nombre
WHERE a.club_cif IS NULL;


-- 6 --
SELECT c.provincia, COUNT(DISTINCT c.rio)
FROM cotos c
GROUP BY c.provincia;

-- 7 --
SELECT c.provincia, COUNT(DISTINCT c.rio)
FROM 
  cotos c
INNER JOIN
  autorizados a
ON c.nombre = a.coto_nombre
GROUP BY provincia;


-- 8 - Provincia con m�s cotos autirizados
SELECT c.provincia, COUNT(c.nombre) AS ncotos
FROM 
  cotos c
INNER JOIN
  autorizados a
ON c.nombre = a.coto_nombre
GROUP BY c.provincia;

SELECT MAX(ncotos) AS value
FROM
(
  SELECT c.provincia, COUNT(c.nombre) AS ncotos
  FROM 
    cotos c
  INNER JOIN
    autorizados a
  ON c.nombre = a.coto_nombre
  GROUP BY c.provincia
) AS maximo;


SELECT c.provincia
FROM 
  cotos c
INNER JOIN
  autorizados a
ON c.nombre = a.coto_nombre
GROUP BY c.provincia
HAVING COUNT(DISTINCT c.nombre) = 
( SELECT MAX(censoCotos.ncotos) AS maximo
  FROM
  ( SELECT c.provincia, COUNT(DISTINCT c.nombre) AS ncotos
    FROM 
      cotos c
    INNER JOIN
      autorizados a
    ON c.nombre = a.coto_nombre
    GROUP BY c.provincia
  ) AS censoCotos
);


SELECT censoCotos.provincia
FROM
( SELECT c.provincia, COUNT(*) AS ncotos
  FROM
    cotos c
  INNER JOIN
    autorizados a
  ON c.nombre = a.coto_nombre
  GROUP BY c.provincia
) AS censoCotos
INNER JOIN
( SELECT MAX(censoCotos.ncotos) AS value
  FROM
  ( SELECT c.provincia, COUNT(DISTINCT c.nombre) AS ncotos
    FROM 
      cotos c
    INNER JOIN
      autorizados a
    ON c.nombre = a.coto_nombre
    GROUP BY c.provincia
  ) AS censoCotos
) AS m�ximo
ON censoCotos.ncotos = m�ximo.value; 


-- 9 - Indica el nombre de los pescadres y de sus ahijados
SELECT padrino.nombre AS padrino,  ahijado.nombre AS ahijado
FROM 
  pescadores padrino
INNER JOIN
  apadrinar a
INNER JOIN
  pescadores ahijado
ON padrino.numSocio = a.padrino AND a.ahijado = ahijado.numSocio;



-- 10 - Numero de apadrinados por cada pescador

SELECT *, COUNT(a.ahijado) AS nahijados
FROM apadrinar a
GROUP BY a.padrino;
