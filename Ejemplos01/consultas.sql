USE concursos;

/*
 * 1- Indica el nombre de los concursantes de Burgos
 */

SELECT DISTINCT 
  nombre
FROM concursantes c
WHERE c.provincia LIKE 'BURGOS';

SELECT DISTINCT 
  nombre
FROM concursantes c
WHERE LOWER(c.provincia) = LOWER('Burgos');

SELECT DISTINCT 
  nombre
FROM concursantes c
WHERE c.provincia IN ('Burgos');


/*
 * 2- Indica el n�mero de concursantes de Burgos
 */

SELECT DISTINCT 
  COUNT(*) AS numconcursantes
FROM concursantes c
WHERE c.provincia = 'BURGOS';


/*
 * 3- Indica el n�mero de concursantes de cada poblaci�n
 */

SELECT DISTINCT 
  -- c.provincia, 
  c.poblacion, COUNT(c.poblacion) AS numconcursantes
FROM concursantes c
GROUP BY c.poblacion;


/*
 * 4- Indicar las poblaciones que tienen concursantes de meons de 90KG
 */
SELECT DISTINCT 
  c.poblacion
FROM concursantes c
WHERE c.peso < 90;


/*
 * 5- Indicar las poblaciones que tienen m�s de un concursante
 */
SELECT -- No hace falta DISTINCT porque al agrupar, el campo por el que agrupas se convierte en campo clave 
  c.poblacion
FROM concursantes c
GROUP BY c.poblacion
HAVING COUNT(*) >1;

SELECT c1.poblacion
FROM
  (
    SELECT DISTINCT 
      c.poblacion, COUNT(*) AS nconcursantes
    FROM concursantes c
    GROUP BY c.poblacion
  ) AS c1
WHERE c1.nconcursantes>1;


/*
 * 6- Indicar las poblaciones que tienen m�s de un concursante de menos de 90kg
 */

SELECT DISTINCT 
  c.poblacion
FROM concursantes c
WHERE c.peso <90
GROUP BY c.poblacion
HAVING COUNT(*) >1;



/* CONSULTA CON WHERE
 * Listame nompre y poblacion de los concursantes que hayan nacido en los meses de verano (JULIO AGOSTO SEPTIEMBRE)
 */

-- PI [nombre,poblacion] (FILTRO [MONTH(fechaNacimiento)] IN 7,8,9 (concursantes) )

SELECT c.nombre, c.poblacion, c.fechaNacimiento
FROM concursantes c 
WHERE MONTH(c.fechaNacimiento) IN(7,8,9);


/* CONSULTA CON HAVING
 * EN QUE A�OS HAN NACIDO MAS DE DOS CONCURSANTES
 */

PI[year(fechaNacimiento), COUNT(*))] [year(fechanacimiento)]G[COUNT(*)](concursantes)

SELECT year(c.fechaNacimiento) AS a�o, COUNT(*) AS numconcursantes
FROM concursantes c 
GROUP BY YEAR(c.fechaNacimiento)
HAVING COUNT(*) >2;


/* WHERE HAVING
 *
 */
SELECT c.provincia, AVG(c.altura)
  FROM concursantes c
  WHERE c.nombre NOT LIKE '%a'
  GROUP BY (c.provincia)
  HAVING AVG(c.altura) > 175;
 
  


/* TOTALES
 * Cuantos hombres hay
 */
SELECT COUNT(*)
  FROM concursantes c
  WHERE c.nombre NOT LIKE '%a';


/* WHERE HAVING TOTALES
 * Cuantos kilos pesan todas las concursantes de cada provincia
 */

SELECT  c.provincia, SUM(c.peso)
  FROM concursantes c
  WHERE c.nombre LIKE '%a'
  GROUP BY (c.provincia)
  HAVING SUM(c.peso) > 300;
