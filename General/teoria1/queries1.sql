USE teoria1;

-- Numero de alquileres del usuario 1
SELECT COUNT(*) 
FROM alquileres al
WHERE al.usuario = 1;

-- Numero de coches alquilados por el usuario 1
 SELECT COUNT(DISTINCT al.coche) 
FROM alquileres al
WHERE al.usuario = 1;

-- Numero de alquileres por mes
SELECT MONTH(al.fecha) mes, COUNT(*) total
  FROM alquileres al 
  GROUP BY MONTH(al.fecha);

-- Numero de clientes por sexo
SELECT u.sexo, COUNT(*) AS numero 
FROM usuarios u 
GROUP BY u.sexo;

-- Numero de alquileres de coches por color
SELECT c.color, COUNT(*) AS alquileres
FROM 
  alquileres a 
INNER JOIN 
  coches c 
ON a.coche = c.codigoCoche
GROUP BY c.color;

-- Numero de cocues que se han alquilado, por colores
SELECT c.color, COUNT(DISTINCT c.codigoCoche) AS alquileres
FROM 
  alquileres a 
INNER JOIN 
  coches c 
ON a.coche = c.codigoCoche
GROUP BY c.color;

-- Numero de marcas 
SELECT COUNT(DISTINCT c.marca) 
FROM coches c;

-- Numero de marcas de coches alquilados
SELECT COUNT(DISTINCT c.marca) AS marcas_alquiladas
FROM 
  alquileres al 
INNER JOIN 
  coches c 
ON al.coche = c.codigoCoche;

-- Numero de alquileres, por marca
SELECT c.marca, COUNT(*)  alquileres
FROM 
  alquileres a 
INNER JOIN 
  coches c 
ON a.coche = c.codigoCoche
GROUP BY c.marca;

-- Numero de coches distintos alquilados por marca
SELECT c.marca, COUNT(DISTINCT c.codigoCoche) AS alquilados
FROM 
  alquileres a 
INNER JOIN 
  coches c 
ON a.coche = c.codigoCoche
GROUP BY c.marca;

-- Numero de coches disponibles por marca
SELECT c.marca, COUNT(*) AS disponibles
FROM coches c 
GROUP BY c.marca;
