USE teoria1;

-- 1 - Indicar el nombre de las marcas que hayan alquilado coches

SELECT DISTINCT c.marca
FROM 
  alquileres a 
  INNER JOIN 
  coches c 
  ON a.coche = c.codigoCoche
;

-- Optimizar para velocidad
-- Reducier el n�mero de choches alquilados de alquileres
SELECT DISTINCT  a.coche FROM alquileres a;

-- Consulta join optimizado
SELECT DISTINCT c.marca
FROM 
( SELECT DISTINCT a.coche FROM alquileres a
) AS a
  INNER JOIN 
  coches c 
  ON a.coche = c.codigoCoche
;


-- 2 - Nombres de los usuarios que hayan alquilado alguna vez coches 
SELECT DISTINCT u.nombre
FROM 
  alquileres a  
  INNER JOIN 
  usuarios u 
  ON a.usuario = u.codigoUsuario;


-- Optimizada

-- Reducir el n�mero de choches alquilados de alquileres
SELECT DISTINCT  a.usuario FROM alquileres a;


SELECT DISTINCT u.nombre
FROM 
  (
    SELECT DISTINCT a.usuario 
    FROM alquileres a 
  ) AS a
  INNER JOIN 
  usuarios u 
  ON a.usuario = u.codigoUsuario
;




-- 3 - Coches que no han sido alquilados

-- Resta coches - alquileres
-- PIcodigocoche(coches) - PIcoche(alquileres)
/*
SELECT c.codigoCoche FROM coches c
MINUS
SELECT a.coche FROM  alquileres a;
*/

-- alquilados
SELECT DISTINCT a.coche 
FROM alquileres a;


-- NOT IN
SELECT c.codigoCoche
FROM coches c 
WHERE c.codigoCoche NOT IN 
  ( SELECT DISTINCT a.coche 
    FROM alquileres a
  );


-- LEFT JOIN
SELECT c.* -- DISTINCT c.marca
FROM 
  coches c 
  LEFT JOIN 
  alquileres a 
  ON a.coche = c.codigoCoche
WHERE a.codigoAlquiler IS NULL;


-- Optimizada
SELECT c.*
FROM
  coches c
  LEFT JOIN
  ( SELECT DISTINCT a.coche 
    FROM alquileres a
  ) a
  ON c.codigocoche = a.coche
WHERE a.coche IS NULL;



-- 4 - Clientes que no han alquilado coches
-- PIcodigocliente(cliente) - PIcliente(alquileres)

-- alquilantes
SELECT DISTINCT a.usuario
FROM alquileres a;

-- NOT IN
SELECT * 
FROM usuarios u 
WHERE u.codigoUsuario NOT IN 
( SELECT DISTINCT a.usuario 
  FROM alquileres a
);

-- NOT EXIST
SELECT *
FROM usuarios u
WHERE NOT EXISTS
(
  SELECT NULL FROM alquileres a WHERE a.usuario = u.codigoUsuario
);


-- LEFT JOIN
SELECT u.* 
FROM 
  usuarios u 
  LEFT JOIN 
  alquileres a 
  ON u.codigoUsuario = a.usuario 
WHERE a.codigoAlquiler IS null;