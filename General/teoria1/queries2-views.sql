USE teoria1;

/****

VIEWS - CHECKS y ASERCIONES

 - check es una regla de validaci�n de formato, autocontenida en el CAMPO INDIVIDUAL.

 - aserciones es una regla de validacion que envuelve m�s de un campo e incluso una query

Los checks y las aserciones no funcionan en MySQL, incluido MySQL 8 (5.8).
Esto significa que se puede implementar con triggers.

Operaciones con tablas:
  Select
  Insert
  Update
  Delete
Los triggers son programas que se ejecutan entes y despues de cada operaci�n.

Cosas hechas con programaci�n:

  Triggers (disparadores) 
  Eventos


�Como hacer parte del trabajo de los triggers sin programar? Usa vistas (views)

****/


-- Ejemplo de check
CREATE OR ALTER TABLE viajes (
  id int(11) AUTO_INCREMENT,
  ffin date,
  fini date,
  CHECK (ffin<fini), -- asercion, porque envuelve m�s de un campo
  PRIMARY KEY (id)
);


             



-- 1 - Indicar el nombre de las marcas que hayan alquilado coches

SELECT DISTINCT c.marca
FROM 
  alquileres a 
  INNER JOIN 
  coches c 
  ON a.coche = c.codigoCoche
;

-- Optimizar para velocidad
-- Reducier el n�mero de choches alquilados de alquileres
SELECT DISTINCT  a.coche FROM alquileres a;

-- Consulta join optimizado
SELECT DISTINCT c.marca
FROM 
( SELECT DISTINCT a.coche FROM alquileres a
) AS a
  INNER JOIN 
  coches c 
  ON a.coche = c.codigoCoche
;


-- CREAR UNA VISTA
CREATE OR REPLACE VIEW consulta1 AS
  SELECT DISTINCT c.marca
  FROM 
    alquileres a 
    INNER JOIN 
    coches c 
    ON a.coche = c.codigoCoche
  ;

SELECT * FROM consulta1 c;



-- Soporte de vistas con consultas anidadas
CREATE OR REPLACE VIEW consulta1 AS
  SELECT DISTINCT c.marca
  FROM 
  ( SELECT DISTINCT a.coche FROM alquileres a
  ) AS a
    INNER JOIN 
    coches c 
    ON a.coche = c.codigoCoche
;
SELECT * FROM consulta1 c;


-- Consulta no anidada
CREATE OR REPLACE VIEW aConsulta1 AS
  SELECT DISTINCT a.coche FROM alquileres a;
CREATE OR REPLACE VIEW consulta1 AS
  SELECT DISTINCT c.marca
  FROM 
    aConsulta1 a
    INNER JOIN 
    coches c 
    ON a.coche = c.codigoCoche
;
SELECT * FROM consulta1 c;



-- 2 - Nombres de los usuarios que hayan alquilado alguna vez coches 
CREATE OR REPLACE VIEW aConsulta2 AS
  SELECT DISTINCT  a.usuario FROM alquileres a;

CREATE OR REPLACE VIEW consulta2 AS
SELECT DISTINCT u.nombre
FROM 
  aConsulta2 a
  INNER JOIN 
  usuarios u 
  ON a.usuario = u.codigoUsuario
;

SELECT * FROM consulta2 c;



-- 3 - Coches que no han sido alquilados
CREATE OR REPLACE VIEW cochesAlquilados AS
  SELECT DISTINCT a.coche 
  FROM alquileres a;


-- NOT IN
CREATE OR REPLACE VIEW cochesNoAlquilados AS
SELECT c.codigoCoche
FROM coches c 
WHERE c.codigoCoche NOT IN 
  ( SELECT * FROM cochesAlquilados)
;

CREATE OR REPLACE VIEW cochesNoAlquilados AS
SELECT c.*
FROM
  coches c
  LEFT JOIN
  ( SELECT * FROM cochesAlquilados a
  ) a
  ON c.codigocoche = a.coche
WHERE a.coche IS NULL;



SELECT * FROM cochesNoAlquilados cna;




-- 4 - Clientes que no han alquilado coches
-- PIcodigocliente(cliente) - PIcliente(alquileres)

-- alquilantes
CREATE OR REPLACE VIEW alquilantes AS
SELECT DISTINCT a.usuario
FROM alquileres a;

-- NOT IN
CREATE OR REPLACE VIEW noAlquilantes AS
SELECT * 
FROM usuarios u 
WHERE u.codigoUsuario NOT IN 
( SELECT * FROM alquilantes);


-- LEFT JOIN
CREATE OR REPLACE VIEW noAlquilantes AS
SELECT u.* 
FROM 
  usuarios u 
  LEFT JOIN 
  ( SELECT a.usuario FROM alquileres a
  ) a
  ON u.codigoUsuario = a.usuario
WHERE a.usuario IS null;

SELECT * FROM noAlquilantes a;