﻿USE ciclistas;

/*
  SELECT provincia, COUNT(*)
  FROM localidades
  GROUP BY provincia;


-- Primero se hace el where antes que los cálculos como count
SELECT provincia, COUNT(*)
  FROM localidades
  GROUP BY provincia;


-- Dos opciones: Subconsulta, y hacer el where fuera.
-- Usar having, es como where, pero se ejecuta despues del group by y le afectan las funciones.

  */


-- ALTURA MEDIA DE LOS PUERTOS, POR CATEGORIA
  SELECT AVG(altura),  p.categoria
  FROM puerto p
    GROUP BY p.categoria
    HAVING AVG(p.altura)>2000;

-- Categorias y la altura media de sus puertos, cuendo esta sea mayor que dosmiol
  SELECT AVG(altura) AS alturaMedia,  p.categoria
  FROM puerto p
    GROUP BY p.categoria
    HAVING alturaMedia>2000;

-- altura
  SELECT p.categoria
  FROM puerto p
    GROUP BY p.categoria
    HAVING AVG(altura) > 2000;



-- ALTURA MEDIA DE LOS PUERTOS POR CATEGORIA, PERO SOLAMENTE SI EL PUERTO ES MAYOR DE 1500M

  SELECT p.categoria, AVG(p.altura)
  FROM
    puerto p
  GROUP BY p.categoria;

  SELECT p.categoria, AVG(p.altura) AS altura_media, COUNT(*) AS num_puertos
  FROM
    puerto p
  WHERE p.altura > 1500
  GROUP BY p.categoria;


-- ALTURA MEDIA DE LOS PUERTOS mayoresde 1500 POR CATEGORIA, PERO SOLAMENTE SI la media de alturaes  MAYOR DE 1500M

  SELECT p.categoria, AVG(p.altura) AS altura_media, COUNT(*) AS num_puertos
  FROM
    puerto p
  WHERE p.altura > 1500
  GROUP BY p.categoria;
