﻿USE  ciclistas;

-- Ejemplos a fecha de 2019-07-01, según ejemplos de E-R por ingenieria inversa.

-- Ciclistas que han ganado etapas, mostrar dorsal
/*
PI _dorsal (
  etapas
  )
*/
/*
  SELECT DISTINCT e.dorsal FROM etapa e;
*/

-- Ciclistas que han ganado etapas, mostrar dorsal
/*
PI _dorsal (
    puertos
  )
*/
/*
  SELECT DISTINCT p.dorsal FROM puerto p;
*/


-- Ciclistas que han ganado etapas o puertos, mostrar dorsal
/*
  PI _dorsal (etapa e) UNION PI _dorsal (puerto p)
  */
/*
  SELECT DISTINCT e.dorsal FROM etapa e
  UNION 
  SELECT DISTINCT p.dorsal FROM puerto p
;
*/

-- Ciclistas que han ganado etapas o puertos, mostrar dorsal
/*
  PI _dorsal (etapa e) INTERSECT PI _dorsal (puerto p)
  */
/*
  SELECT DISTINCT e.dorsal FROM etapa e
  INTERSECT 
  SELECT DISTINCT p.dorsal FROM puerto p;
*/
/*
SELECT * FROM 
  (SELECT DISTINCT e.dorsal FROM etapa e) c1
JOIN
  (SELECT DISTINCT p.dorsal FROM puerto p) c2
 ON c1.dorsal = c2.dorsal;
*/

-- Solucionar ambiguedad de .dorsal
/*
SELECT 
  * 
FROM 
  (SELECT DISTINCT e.dorsal AS dorsalEtapa FROM etapa e) c1
JOIN
  (SELECT DISTINCT p.dorsal AS dorsalPuerto FROM puerto p) c2
ON c1.dorsalEtapa = c2.dorsalPuerto;
*/

SELECT 
  dorsalEtapa AS dorsal
FROM 
  (SELECT DISTINCT e.dorsal AS dorsalEtapa FROM etapa e) c1
JOIN
  (SELECT DISTINCT p.dorsal AS dorsalPuerto FROM puerto p) c2
ON c1.dorsalEtapa = c2.dorsalPuerto;

;

*/
--  SELECT * FROM ciclista c JOIN etapa e ON c.dorsal = e.dorsal JOIN puerto p ON c.dorsal = p.dorsal WHERE e.dorsal = p.dorsal;


-- SELECT DISTINCT e.dorsal FROM etapa e JOIN puerto p ON e.dorsal = p.dorsal;


--  A   B   A union B   A intersect B   A,B o (X)
-- 16  11  24           3=16+11-24      176=16*11


-- Listado de todas las etapas, y los datos del ciclista que las ha ganado
--  SELECT * FROM etapa e  JOIN ciclista c ON e.dorsal = c.dorsal;


-- Listado de todos los puertos, y los datos del ciclista que los ha ganado
--  SELECT * FROM puerto p  JOIN ciclista c ON p.dorsal = c.dorsal;



/*
-- PRODUCTO CARTESIANO Y WHERE
  SELECT DISTINCT e.dorsal FROM etapa e, puerto p WHERE e.dorsal = p.dorsal;


COMBINACION INTERNA ([INNER] JOIN)
  SELECT * FROM ciclista c JOIN puerto p ON c.dorsal = puertos.p;
  SELECT * FROM A,B = SELECT * A JOIN B,
*/


/*
 *  COMBINACION INTERNA ([INNER] JOIN)
 */
SELECT 
  * 
FROM
  equipo e INNER JOIN ciclista c 
  ON e.nomequipo = c.nomequipo;

-- ¿Que lista? Ciclistas, porque esta combinacion es una relacion (FOREIGN KEY) ºPORQUE ESTA RELACIÓN ES UN FOREIGN KEY equipo <1:N> ciclista
-- LISTADO DE CICLISTA CON LOS DATOS DEL EQUIPO AL QUE PERTENECEN


SELECT 
  * 
FROM
  equipo e INNER JOIN ciclista c 
  USING (nomequipo);

-- Con USING, el campo para sincronizar debe llamarse en ambos igual, y elimina la duplicación del campo de conexión.


SELECT 
  * 
FROM
  equipo e INNER JOIN ciclista c 
WHERE 
  e.nomequipo = c.nomequipo;

-- Es válido pero mejor no utuilizarlo, dado que es básicamente el producto cartesiano

SELECT 
  * 
FROM
  equipo e, ciclista c 
WHERE 
  e.nomequipo = c.nomequipo;



-- Otros ejemplos


-- Lista los nombres de los ciclistas que han ganado puertos, el puerto ganado, y su equipo
SELECT 
  p.nompuerto,
  c.nombre,
  c.nomequipo
FROM
  puerto p INNER JOIN ciclista c
  USING (dorsal);
-- 21, mostrando repetidos


-- Lista los nombres de los ciclistas y del equipo, de aquellos ciclistas que hayan ganado puertos
SELECT DISTINCT
  c.nombre,
  c.nomequipo
FROM
  puerto p INNER JOIN ciclista c
  USING (dorsal);
-- 14 resultados


-- Que ciclistas han ganado puertos, y cuantos puertos han ganado
SELECT DISTINCT
  c.nombre,
  c.nomequipo
FROM
  puerto p INNER JOIN ciclista c
  USING (dorsal)
  GROUP BY c.nombre;




-- Lista los nombres de los ciclistas y del equipo, de aquellos ciclistas que hayan ganado etapas

SELECT DISTINCT
  c.nombre,
  c.nomequipo
FROM
  etapa e INNER JOIN ciclista c
  USING (dorsal);

-- Optimización

-- Ciclistas que han ganado etapas (C1) (21 etapas, 16 ganadores distintos)
-- C1 <- PI_dorsal(etapas)
SELECT DISTINCT 
  e.dorsal 
FROM 
  etapa e;

SELECT -- [DISTINCT] es opcional aquí, porque C1 ya es única, el join solo añade columnas, pero no filas
  c.nombre,
  c.nomequipo
FROM
  (SELECT DISTINCT e.dorsal FROM etapa e) c1 INNER JOIN ciclista c
  USING (dorsal);


/* Quiero saber los ciclistas que han ganado puertos, 
 * y el número de puertos que han ganado. Del ciclista 
 * tiene que saber dorsal y nombre.
 *
 * Salida(dorsal, nombre, veces) - Vecessiempre es mayor que 0
 */

-- Ciclistas que han ganado puertos: puerto
SELECT 
  * 
FROM 
  puerto p;

-- ciclistas qye han ganado puertos y sus datos: C1 := puerto (x)_dorsal ciclista
SELECT DISTINCT 
  * 
FROM 
  puerto p JOIN ciclista c 
USING (dorsal);

-- Filtrar columnas: C1 := PI_dorsal, nombre, nompuerto ( puertos (x)_dorsal ciclistas )
SELECT DISTINCT 
  c.dorsal, c.nombre, p.nompuerto 
FROM 
  puerto p JOIN ciclista c 
USING (dorsal);
  -- Se mete nompuertos porque es primary key de los puertos, para que los registros 
  -- no salgan duplicados en el caso de corredores que ganan varios puertos y fallen 
  -- los totales


-- Agrupo por dorsales,nombres contando los registros (distintos nompuertos)

SELECT 
  * -- c1.dorsal, c1.nombre, COUNT(*) AS numeroPuertos
FROM (
  SELECT DISTINCT 
    c.dorsal, c.nombre, p.nompuerto 
  FROM 
    puerto p JOIN ciclista c 
  USING (dorsal)
  ) AS c1
GROUP BY c1.dorsal, c1.nombre;

  -- dorsal,nombre_Groups_count(*) as veces (C1)


    
/* 
 * Cuantas etapas ha ganado cada ciclista
 *  DORSAL/NUM_ETAPAS
 */

SELECT e.dorsal, COUNT(e.dorsal) AS numero_etapas
FROM
  etapa e
GROUP BY e.dorsal;


-- Con subquery
SELECT c.nombre
FROM (
    SELECT e.dorsal
    FROM
      etapa e 
    GROUP BY e.dorsal
    HAVING COUNT(*)>2
  ) ce INNER JOIN ciclista c 
USING (dorsal);


-- Sin subquery
SELECT e.dorsal, c.nombre, COUNT(*) AS numero_etapas
FROM
  etapa e INNER JOIN ciclista c USING (dorsal)
GROUP BY e.dorsal
HAVING numero_etapas>2;


--El nombre del ciclista que tiene mas edad

-- Edad máxima del ciclista
SELECT MAX(c.edad) AS maxima FROM ciclista c;

SELECT *
FROM 
  ciclista c 
  INNER JOIN 
  (SELECT MAX(c.edad) AS maxima FROM ciclista c) c1
ON c1.maxima = c.edad;


SELECT c.nombre
FROM 
  ciclista c 
  INNER JOIN 
  (SELECT MAX(c.edad) AS maxima FROM ciclista c) c1
ON c1.maxima = c.edad;


/*
  c1 := GROUP_MAX(EDAD)=maxima(ciclista)
  PI{nombre} ( c1 (X)_(maxima=edad) ciclista )
*/


/*
  PI{nombre} (F_edad = c1 (ciclistas)
  */

SELECT c.nombre
FROM 
  ciclista c 
WHERE
  c.edad = (SELECT MAX(c.edad) FROM ciclista c);
