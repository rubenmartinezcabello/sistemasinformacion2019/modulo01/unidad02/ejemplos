﻿USE ciclistas;



-- 1.1 
/*
SELECT DISTINCT 
  c.edad 
FROM 
  ciclista c;
*/



-- 1.2 
/*
SELECT DISTINCT 
  c.edad 
FROM 
  ciclista c
WHERE c.nomequipo = 'Artiach';
*/
/*
SELECT DISTINCT 
  c.edad 
FROM 
  ciclista c
WHERE c.nomequipo LIKE 'Artiach';
*/

-- 1.3
/*
SELECT DISTINCT 
  c.edad
FROM 
  ciclista c
WHERE
  c.nomequipo = 'Artiach'
  OR
  c.nomequipo = 'Amore Vita';
*/
/*
SELECT DISTINCT 
  c.edad
FROM 
  ciclista c
WHERE
  c.nomequipo LIKE 'Artiach'
  OR
  c.nomequipo LIKE 'Amore Vita';
*/
/*
SELECT DISTINCT 
  c.edad
FROM 
  ciclista c
WHERE
  c.nomequipo IN ('Artiach','Amore Vita');
*/

-- 1.4
--  Distinct es redundante porque es un campo clave, que ya es único.
/*
SELECT
  c.dorsal
FROM  
  ciclista c 
WHERE edad<25 OR edad>30;
*/
/*
SELECT 
  c.dorsal
FROM
  ciclista c
WHERE NOT edad BETWEEN 25 AND 30;
*/

-- 1.5
/*
SELECT
  c.dorsal
FROM
  ciclista c
WHERE c.edad >=28 AND c.edad <=32 AND c.nomequipo = 'Banesto';
*/
/*
SELECT
  c.dorsal
FROM
  ciclista c
WHERE c.edad BETWEEN 28 AND 32 AND c.nomequipo LIKE 'Banesto';
*/

-- 1.6
/*
SELECT DISTINCT 
  c.nombre
FROM
  ciclista c
WHERE
  CHAR_LENGTH(c.nombre)>8;
*/
/*
SELECT DISTINCT 
  c.nombre
FROM
  ciclista c
WHERE
  NOT c.nombre LIKE '_________%';
*/

/*
SELECT *
FROM 
  puerto p
WHERE
  altura>1500;
*/

SELECT 
  p.dorsal
FROM 
  puerto p
WHERE
  altura BETWEEN 1800 AND 3000
UNION
SELECT 
  p.dorsal
FROM 
  puerto p 
WHERE
  p.pendiente>8;

SELECT * 
FROM
  (SELECT  DISTINCT p.* FROM puerto p WHERE p.pendiente>8) AS pen
  INNER JOIN
  (SELECT  DISTINCT p.* FROM puerto p WHERE altura BETWEEN 1800 AND 3000) AS alt
  USING (dorsal);


SELECT COUNT(*) AS total
  FROM ciclista c
  ;

SELECT
  COUNT(*)
FROM
  ciclista c
WHERE
  c.nomequipo LIKE 'BANESTO';

SELECT
  AVG(c.edad)
FROM 
  ciclista c
WHERE
  c.nomequipo LIKE 'BANESTO';

SELECT 
  c.nomequipo, AVG(edad) AS edadmedia
FROM ciclista c
GROUP BY (c.nomequipo);

SELECT DISTINCT 
  c.nomequipo, COUNT(*) AS numcorredores
FROM ciclista c
GROUP BY c.nomequipo;

SELECT
  COUNT(*)
FROM puerto p;

SELECT
  COUNT(*)
FROM puerto p
WHERE altura>1500;

SELECT 
  c.nomequipo, COUNT(*) AS numcorredores
FROM ciclista c
GROUP BY c.nomequipo
HAVING numcorredores>4 ;

SELECT 
  c.nomequipo, COUNT(*) AS numcorredores
FROM ciclista c
WHERE edad BETWEEN 28 AND 32
GROUP BY c.nomequipo
HAVING numcorredores>4;

SELECT 
  e.dorsal, COUNT(*) AS numvictorias
FROM etapa e
GROUP BY e.dorsal;
SELECT 
  e.dorsal, COUNT(*) AS numvictorias
FROM etapa e
GROUP BY e.dorsal
HAVING numvictorias>1;


SELECT
  ci.*
FROM
  (SELECT DISTINCT l.dorsal FROM lleva l WHERE l.código LIKE 'MGE') ma
  INNER JOIN
  (SELECT * FROM ciclista c) ci
  ON ma.dorsal = ci.dorsal
;